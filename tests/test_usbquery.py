# Copyright (C) 2023 Codethink Ltd.

from tiab.usbquery import Query, parse
from tiab.usbquery.ast import All, Any, Not, Parent, PortNumber, ProductID, VendorID


def test_usbquery_parser() -> None:
    assert parse("vid 1") == VendorID(1)
    assert parse("pid 0xab") == ProductID(0xAB)
    assert parse("port 3") == PortNumber(3)

    ast: Query = All([VendorID(0x1234), ProductID(0xABCD)])
    assert parse("usbid 1234:abcd") == ast
    assert parse("vid 0x1234 && pid 0xabcd") == ast

    assert parse("!(vid 1)") == Not(VendorID(1))
    assert parse("vid 1 && pid 2") == All([VendorID(1), ProductID(2)])
    assert parse("vid 1 || pid 2") == Any([VendorID(1), ProductID(2)])

    assert parse("parent vid 1") == Parent(VendorID(1))
