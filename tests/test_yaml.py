# Copyright (C) 2023 Codethink Ltd.

from pathlib import Path

import strictyaml as sy
from tiab.hardware.desc import UsbSerialDesc
from tiab.hardware.ftdi_yaml import parse_ftdi_firmware_config
from tiab.hardware.hw_yaml import parse_hardware_desc
from tiab.usbquery import ast as usbquery_ast
from tiab.utils.yaml import yaml_cast


def test_yaml_cast() -> None:
    int_yaml = sy.load("123")
    hex_yaml = sy.load("0x123")
    float_yaml = sy.load("1.23")
    str_yaml = sy.load("foo")
    bool_yaml = sy.load("yes")
    list_yaml = sy.load("""- 1\n- 2\n- 3\n""")

    assert yaml_cast(int, int_yaml) == 123  # noqa: PLR2004
    assert yaml_cast(int, hex_yaml) == 0x123  # noqa: PLR2004
    assert yaml_cast(float, float_yaml) == 1.23  # noqa: PLR2004
    assert yaml_cast(str, str_yaml) == "foo"
    assert yaml_cast(bool, bool_yaml)
    assert yaml_cast(list[int], list_yaml) == [1, 2, 3]
    assert yaml_cast(list[str], list_yaml) == ["1", "2", "3"]


def test_ftdi_firmware_config_yaml() -> None:
    yaml = sy.load(
        """
properties:
  vendor-id: 0x1234
  product-id: 0x5678
  manufacturer: mfg
  product: prod
  serial: serial_nr
  cbus-func-0: func0
  cbus-func-1: func1
  cbus-func-2: func2
  cbus-func-3: func3
  cbus-func-4: func4
  cbus-func-5: func5
  cbus-func-6: func6
  cbus-func-7: func7
  cbus-func-8: func8
  cbus-func-9: func9

ini-path: /path/to/config.ini
ini-section: all
eeprom-size: 256
ignore-existing-data: no
""",
    )

    config = parse_ftdi_firmware_config(yaml)

    assert config.properties["vendor_id"] == 0x1234  # noqa: PLR2004
    assert config.properties["product_id"] == 0x5678  # noqa: PLR2004
    assert config.properties["manufacturer"] == "mfg"
    assert config.properties["product"] == "prod"
    assert config.properties["serial"] == "serial_nr"
    assert config.properties["cbus_func_0"] == "func0"
    assert config.properties["cbus_func_1"] == "func1"
    assert config.properties["cbus_func_2"] == "func2"
    assert config.properties["cbus_func_3"] == "func3"
    assert config.properties["cbus_func_4"] == "func4"
    assert config.properties["cbus_func_5"] == "func5"
    assert config.properties["cbus_func_6"] == "func6"
    assert config.properties["cbus_func_7"] == "func7"
    assert config.properties["cbus_func_8"] == "func8"
    assert config.properties["cbus_func_9"] == "func9"

    assert config.ini_path
    assert config.ini_path == Path("/path/to/config.ini")

    assert config.ini_section
    assert config.ini_section == "all"

    assert config.eeprom_size
    assert config.eeprom_size == 256  # noqa: PLR2004

    assert not config.ignore_existing_data


def test_hardware_desc_yaml() -> None:
    yaml = sy.load(
        """
usb-queries:
  one: port 0
  two: port 1

usb-parents:
  one: two

serial-descs:
  serial1:
    type: usb
    device: one
""",
    )

    desc = parse_hardware_desc(yaml)

    assert desc.usb_queries == {
        "one": usbquery_ast.PortNumber(0),
        "two": usbquery_ast.PortNumber(1),
    }

    assert desc.usb_parents == {
        "one": "two",
    }

    assert desc.serial_descs == {
        "serial1": UsbSerialDesc(device="one"),
    }
