## License information

Code added by Codethink Limited to this repository is licensed under
[MIT licensing](/MIT_licensing.txt)
