# Copyright (C) 2023 Codethink Ltd.

"""Defines the AST for the USB query language."""

from __future__ import annotations

from dataclasses import dataclass
from typing import Union

from tiab.utils.dicttree import DictNode


@dataclass
class VendorID:
    vendor_id: int


@dataclass
class ProductID:
    product_id: int


@dataclass
class PortNumber:
    port_number: int


@dataclass
class All:
    queries: list[Query]


@dataclass
class Any:
    queries: list[Query]


@dataclass
class Not:
    query: Query


@dataclass
class Parent:
    """
    Parent matches if its argument query matches the parent device.

    The match fails if there is no parent device (this only happens
    for root ports).
    """

    query: Query


Query = Union[VendorID, ProductID, PortNumber, All, Any, Not, Parent]
QueryNode = DictNode[str, Query]
