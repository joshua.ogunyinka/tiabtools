# Copyright (C) 2023 Codethink Ltd.

from __future__ import annotations

from typing import cast

import strictyaml as sy
from lark import Lark, Token, Transformer

from tiab.utils.yaml import yaml_cast

from . import ast
from .ast import Query, QueryNode

GRAMMAR = """
query: "vid" INTEGER                -> vendor_id_eq
     | "pid" INTEGER                -> product_id_eq
     | "usbid" HEXINT ":" HEXINT    -> usbid_eq
     | "port" INTEGER               -> port_number_eq
     | "(" query ")"                -> parens
     | "!" query                    -> logical_not
     | query "&&"? query            -> logical_and
     | query "||" query             -> logical_or
     | "parent" query               -> parent

INTEGER: INT | ("0x" | "0X") HEXDIGIT+
HEXINT: HEXDIGIT+

%import common.INT
%import common.HEXDIGIT
%import common.WS
%ignore WS
"""


class USBQueryTransformer(Transformer):
    def vendor_id_eq(self, items: list[Token]) -> Query:
        return ast.VendorID(int(items[0], base=0))

    def product_id_eq(self, items: list[Token]) -> Query:
        return ast.ProductID(int(items[0], base=0))

    def port_number_eq(self, items: list[Token]) -> Query:
        return ast.PortNumber(int(items[0], base=0))

    def usbid_eq(self, items: list[Token]) -> Query:
        return ast.All(
            [
                ast.VendorID(int(items[0], base=16)),
                ast.ProductID(int(items[1], base=16)),
            ],
        )

    def parens(self, items: list[Query]) -> Query:
        return items[0]

    def logical_not(self, items: list[Query]) -> Query:
        return ast.Not(items[0])

    def logical_and(self, items: list[Query]) -> Query:
        return ast.All(items)

    def logical_or(self, items: list[Query]) -> Query:
        return ast.Any(items)

    def parent(self, items: list[Query]) -> Query:
        return ast.Parent(items[0])


def parse(query: str) -> Query:
    """Parse a USB query string into an executable query."""

    parser = Lark(GRAMMAR, start="query")
    tree = parser.parse(query)
    xform = USBQueryTransformer()

    return cast(Query, xform.transform(tree))


##
# YAML tree query parser
##

#: Leaf devices are represented by a query string
SCHEMA_LEAF_DEVICE = sy.Str()

#: Hub devices include a query and a list of child devices.
#: Technically it *is* possible to create a recusive schema
#: by setting MapPattern._value_validator after construction
#: but that's not very nice.
SCHEMA_HUB_DEVICE = sy.Map(
    {
        "query": sy.Str(),
        # Actually SCHEMA_QUERYTREE
        "devices": sy.MapPattern(sy.Str(), sy.Any()),
    },
)

#: Devices can be a leaf or a hub
SCHEMA_DEVICE = SCHEMA_LEAF_DEVICE | SCHEMA_HUB_DEVICE

#: A query node is a mapping of device names -> device queries
SCHEMA_QUERYTREE = sy.MapPattern(sy.Str(), SCHEMA_DEVICE)


def parse_tree(yaml: sy.YAML) -> QueryNode:
    """
    Parse a query tree node from its YAML representation
    """

    yaml.revalidate(SCHEMA_DEVICE)

    if yaml.is_mapping():
        devices = yaml["devices"]
        devices.revalidate(SCHEMA_QUERYTREE)

        query = yaml_cast(str, yaml["query"])
        node = QueryNode(parse(query))

        for key in devices:
            node[yaml_cast(str, key)] = parse_tree(devices[key])

        return node

    if yaml.is_scalar():
        query = yaml_cast(str, yaml)

        return QueryNode(parse(query))

    msg = "YAML schema error"
    raise AssertionError(msg)
