# Copyright (C) 2023 Codethink Ltd.

"""Implements USB query matching on PyUSB devices"""

from __future__ import annotations

from typing import TYPE_CHECKING

from usb.core import Device

import tiab.utils.treeutils as tu
from tiab.utils.dicttree import DictNode

from .ast import (
    All,
    Any,
    Not,
    Parent,
    PortNumber,
    ProductID,
    Query,
    QueryNode,
    VendorID,
)

if TYPE_CHECKING:
    from collections.abc import Iterable

# TODO: This needs to be decoupled from PyUSB devices so it can be tested
# TODO: or we need to use PyFTDI's usbvirt backend


def match(dev: Device, q: Query) -> bool:
    if isinstance(q, VendorID):
        return dev.idVendor == q.vendor_id

    if isinstance(q, ProductID):
        return dev.idProduct == q.product_id

    if isinstance(q, PortNumber):
        return dev.port_number == q.port_number

    if isinstance(q, All):
        return all(match(dev, sq) for sq in q.queries)

    if isinstance(q, Any):
        return any(match(dev, sq) for sq in q.queries)

    if isinstance(q, Not):
        return not match(dev, q.query)

    if isinstance(q, Parent):
        if dev.parent is None:
            return False

        return match(dev.parent, q.query)


def filter_match(q: Query, devices: Iterable[Device]) -> Iterable[Device]:
    """
    Filter an iterable of USB devices and return those matching a query.

    The only reason this exists is because mypy stubbornly refuses to
    infer types for lambda functions, so you can't write code like this:

        filter(lambda d: match(d, q), devices)
    """

    def func(dev: Device) -> bool:
        return match(dev, q)

    return filter(func, devices)


##
# TREE MATCHING IMPLEMENTATION
##

# TODO: There really needs to be a logger for debugging tree queries

DeviceNode = DictNode[str, Device]
DeviceNodeMap = dict[str, list[DeviceNode]]
DeviceNodeEntry = tuple[str, list[DeviceNode]]

#: Used as the key for a DeviceNodeEntry when processing a root device.
#: We don't have to worry about the value of this key because there can
#: only be one root node.
DUMMY_KEY = "__root__"


class TreeMatcher:
    _states: list[DeviceNodeMap]
    _devices: list[Device]

    def __init__(self, devices: Iterable[Device]) -> None:
        self._states = []
        self._devices = list(devices)

    @property
    def _state(self) -> DeviceNodeMap:
        return self._states[-1]

    def _push_state(self) -> None:
        self._states.append({})

    def _pop_state(self) -> DeviceNodeMap:
        return self._states.pop()

    @staticmethod
    def nth_parent_query(q: Query, n: int) -> Query:
        while n > 0:
            q = Parent(q)
            n -= 1

        return q

    @staticmethod
    def _build_leaf_query(qnode: QueryNode) -> Query:
        queries = []
        level = 0

        for qn in qnode.traverse_parents(include_self=True):
            queries.append(TreeMatcher.nth_parent_query(qn.value, level))
            level += 1

        return All(queries)

    def _match_leaf_device(self, qnode: QueryNode) -> DeviceNodeEntry:
        query = TreeMatcher._build_leaf_query(qnode)
        devices = filter_match(query, self._devices)

        return qnode.key or DUMMY_KEY, list(map(DeviceNode, devices))

    def _match_hub_device(
        self,
        qnode: QueryNode,
        dnodemap: DeviceNodeMap,
    ) -> DeviceNodeEntry:
        # Gather up the set of all unique hub devices.
        # I'm sad the typechecker is too weak to do this with map+filter :(
        hubdevs: set[Device] = set()
        for dnodes in dnodemap.values():
            for dnode in dnodes:
                if dnode.value.parent:
                    hubdevs.add(dnode.value.parent)

        # Now we need to process each potential hub device to ensure it has
        # all of the required children. If any are missing, the hub won't be
        # matched. It wouldn't be hard to extend this to support optional
        # child devices if that's ever needed.
        hubnodes: list[DeviceNode] = []
        for hubdev in hubdevs:
            hubnode = DeviceNode(hubdev)

            # Each key represents a single child device on the hub and we
            # want to populate all of those, so iterate over the keys...
            for key in dnodemap:
                # We can have multiple candidate devices per key but only
                # one of these should be assigned to this particular hub.
                for dnode in dnodemap[key]:
                    # Ignore devices that are not a child of this hub.
                    if dnode.value.parent != hubdev:
                        continue

                    # This device node was already claimed by another hub?
                    # This shouldn't be possible so make it an error.
                    if dnode.parent:
                        msg = "Device node claimed by another hub!?!"
                        raise AssertionError(msg)

                    # This means there are two or more devices that match
                    # the *SAME* key. That should not happen and probably
                    # means the child USB query is not strict enough, but
                    # in any event it means we can't match unambiguously.
                    # Just ignore this hub.
                    if key in hubnode:
                        del hubnode[key]
                        break

                    # Found device for this key, record it and repeat the
                    # process for the next key.
                    hubnode[key] = dnode

                # Match failed: this hub is missing a required child device.
                if key not in hubnode:
                    break
            else:
                # If we did not break out of the loop, the match succeeded!
                hubnodes.append(hubnode)

        return qnode.key or DUMMY_KEY, hubnodes

    def run(self, querytree: QueryNode) -> list[DeviceNode]:
        # Push an initial state for the root node
        self._push_state()

        for event in querytree.traverse():
            if event.type == tu.EventType.VISIT_POST:
                if len(event.node) == 0:
                    # For leaf nodes we just run a simple USB query to gather
                    # all matching devices.
                    key, nodes = self._match_leaf_device(event.node)
                else:
                    # By this point all children of this node have been visited
                    # and their device nodes are recorded in the topmost state,
                    # pass that to the hub matcher.
                    key, nodes = self._match_hub_device(event.node, self._pop_state())

                # Save the device list for later
                self._state[key] = nodes

            elif event.type == tu.EventType.LINK_CHILD:
                # When descending to a deeper level, push a new state
                # to keep track of child devices at this level.
                self._push_state()

        # Any root devices will be listed under DUMMY_KEY
        state = self._pop_state()
        return state[DUMMY_KEY]


def match_tree(querytree: QueryNode, devices: Iterable[Device]) -> list[DeviceNode]:
    return TreeMatcher(devices).run(querytree)
