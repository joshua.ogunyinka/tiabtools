import argparse
from time import sleep

from pyftdi.ftdi import Ftdi

from tiab.hardware.bmp280 import BMP280
from tiab.hardware.ssd1306 import SSD1306Display
from tiab.utils import image_encoder

CODETHINK_VENDOR_ID = 0x27BD
CODETHINK_FTDI_1 = 0x000A
CODETHINK_FTDI_2 = 0x000B
CODETHINK_FTDI_3 = 0x000C


def add_vendor_and_prods() -> None:
    Ftdi.add_custom_vendor(CODETHINK_VENDOR_ID)
    Ftdi.add_custom_product(0x27BD, CODETHINK_FTDI_1)
    Ftdi.add_custom_product(0x27BD, CODETHINK_FTDI_2)
    Ftdi.add_custom_product(0x27BD, CODETHINK_FTDI_3)


def start_displaying(sensor_url: str, display_url: str, interval: int) -> None:
    display = SSD1306Display(display_url, 0x3C)
    sensor = BMP280(sensor_url, 0)

    print("Staring the display loop, type Ctrl+C to stop")
    try:
        while True:
            temp = sensor.read_temp()
            data = image_encoder.encode_text(str(temp))
            display.draw(data)
            sleep(interval)
    except KeyboardInterrupt:
        display.close()
        sensor.close()
        print("Exiting...")


def main() -> None:
    add_vendor_and_prods()

    parser = argparse.ArgumentParser(
        prog="temp2display",
        description="A simple program which reads temperature from BMP280 SPI sensor and "
        "displays it on the SSD1306 I2C screen",
    )
    parser.add_argument(
        "--sensor",
        type=str,
        help="the FTDI url of the BMP280 SPI sensor",
        metavar="<ftdi://vendor:id/port>",
        required=True,
    )
    parser.add_argument(
        "--display",
        type=str,
        help="the FTDI url of the SSD1306 i2c display",
        metavar="<ftdi://vendor:id/port>",
        required=True,
    )
    parser.add_argument(
        "--interval",
        type=int,
        help="temperature update interval",
        metavar="<seconds>",
        required=False,
        default=2,
    )

    args = parser.parse_args()
    start_displaying(args.sensor, args.display, args.interval)
