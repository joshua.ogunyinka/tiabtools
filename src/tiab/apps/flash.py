# Copyright (C) 2023 Codethink Ltd.

from __future__ import annotations

import argparse
from dataclasses import dataclass
from pathlib import Path
from typing import Callable, Protocol

import strictyaml as sy

from tiab.hardware import HardwareDesc, HardwareUnit, ftdi_yaml, hw_yaml, open_hw
from tiab.hardware.ftdi import FtdiFirmwareConfig, FtdiFirmwareIO
from tiab.utils.yaml import yaml_cast


class FlashError(Exception):
    pass


@dataclass
class GlobalOptions:
    dry_run: bool


class Action(Protocol):
    def run(self, hwunit: HardwareUnit, opts: GlobalOptions) -> None:
        ...


class FtdiFlashAction(Action):
    _device: str
    _config: FtdiFirmwareConfig
    _io: FtdiFirmwareIO

    def __init__(self, config: sy.YAML) -> None:
        config.revalidate(
            sy.Map(
                {
                    "action": sy.Str(),
                    sy.Optional("enabled"): sy.Bool(),
                    "device": sy.Str(),
                    "config": sy.Any(),
                },
            ),
        )

        self._device = yaml_cast(str, config["device"])
        self._config = ftdi_yaml.parse_ftdi_firmware_config(config["config"])
        self._io = FtdiFirmwareIO(self._config)

    def run(self, hwunit: HardwareUnit, opts: GlobalOptions) -> None:
        usbdev = hwunit.get_usb_device(self._device)

        self._io.open(usbdev)
        self._io.update_firmware(dry_run=opts.dry_run)
        self._io.close()


class UsbResetAction(Action):
    _devices: list[str]

    def __init__(self, config: sy.YAML) -> None:
        config.revalidate(
            sy.Map(
                {
                    "action": sy.Str(),
                    sy.Optional("enabled"): sy.Bool(),
                    "devices": sy.Seq(sy.Str()),
                },
            ),
        )

        self._devices = yaml_cast(list[str], config["devices"])

    def run(self, hwunit: HardwareUnit, opts: GlobalOptions) -> None:
        for name in self._devices:
            usbdev = hwunit.get_usb_device(name)

            if not opts.dry_run:
                usbdev.reset()


@dataclass
class FlashArgs:
    """Argument data for flash command line interface"""

    # config is not really optional but must be due to python jankiness
    config: Path | None = None
    dry_run: bool = False

    @staticmethod
    def configure_parser(parser: argparse.ArgumentParser) -> None:
        parser.add_argument(
            "--config",
            metavar="<cfg.yaml>",
            required=True,
            type=Path,  # type: ignore [misc]
            help="Config file describing the hardware/firmware",
        )
        parser.add_argument(
            "--dry-run",
            action="store_true",
            help="Do not flash, only describe what would be done",
        )


ACTION_MAP: dict[str, Callable[[sy.YAML], Action]] = {
    "flash-ftdi-eeprom": FtdiFlashAction,
    "usb-reset": UsbResetAction,
}

CONFIG_YAML_SCHEMA = sy.Map(
    {
        "hardware": sy.Any(),
        "flash-steps": sy.Seq(
            sy.MapCombined(
                {
                    "action": sy.Str(),
                    sy.Optional("enabled"): sy.Bool(),
                },
                sy.Str(),
                sy.Any(),
            ),
        ),
    },
)


class FlashCommand:
    _config_path: Path
    _options: GlobalOptions

    _hwdesc: HardwareDesc
    _actions: list[Action]

    def __init__(self, args: FlashArgs) -> None:
        if not args.config:
            msg = "Config file is required"
            raise ValueError(msg)

        self._config_path = args.config
        self._options = GlobalOptions(
            dry_run=args.dry_run,
        )

        self._hwdesc = HardwareDesc()
        self._actions = []

    def _parse_config_yaml(self, yaml: sy.YAML) -> None:
        yaml.revalidate(CONFIG_YAML_SCHEMA)

        self._hwdesc = hw_yaml.parse_hardware_desc(yaml["hardware"])

        for action_yaml in yaml["flash-steps"]:
            action = yaml_cast(str, action_yaml["action"])
            action_class = ACTION_MAP.get(action)

            if action_class is None:
                msg = f"Unknown action {action!r} in config"
                raise FlashError(msg)

            # Skip disabled actions (used for testing, really)
            if "enabled" in action_yaml and not yaml_cast(bool, action_yaml["enabled"]):
                continue

            self._actions.append(action_class(action_yaml))

    def _load_config(self) -> None:
        with self._config_path.open("r") as f:
            yaml = sy.load(f.read())

        self._parse_config_yaml(yaml)

    def _scan_units(self) -> list[HardwareUnit]:
        units = open_hw(self._hwdesc)

        if len(units) == 0:
            msg = "No hardware units detected"
            raise FlashError(msg)

        # TODO: In principle this situation can be handled, but for now it isn't
        if len(units) > 1:
            msg = "Detected multiple hardware units, please ensure only one unit is connected"
            raise FlashError(msg)

        return units

    def run(self) -> None:
        self._load_config()

        if len(self._actions) == 0:
            msg = "No actions defined"
            raise FlashError(msg)

        for unit in self._scan_units():
            for action in self._actions:
                action.run(unit, self._options)


def main() -> None:
    parser = argparse.ArgumentParser(
        prog="tiabflash",
        description="Testing-in-a-box flash tool",
    )

    FlashArgs.configure_parser(parser)

    args = parser.parse_args(namespace=FlashArgs())

    FlashCommand(args).run()
