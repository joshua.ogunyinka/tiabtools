# Copyright (C) 2023 Codethink Ltd.
"""
usbquerydemo allows you to test single USB queries or tree queries.

## Query language

USB queries are written in a simple domain-specific language based
on boolean tests:

    query-atom ::= "usbid" HEX-INT ":" HEX-INT
                 | "vid" INTEGER
                 | "pid" INTEGER
                 | "port" INTEGER
                 | "parent" query

    query ::= query-atom
            | query "&&" query
            | query "||" query
            | "(" query ")"
            | "!" query

The vid/pid queries match if the device vendor/product ID equals the
given value. "usbid VEND:PROD" is short for "vid VEND && pid PROD" as
most queries involve a check on the vendor and product ID.

"port N" matches a device if it is plugged into port N of its parent
USB hub. Hub ports are numbered starting from 1. The mapping between
physical ports and port numbers is stable and defined by the USB hub.

"parent QUERY" matches a device D if QUERY matches D's parent device.
This query is rarely used directly, but internally it implements tree
queries (which match a consistent hierarchy of devices).

To test a single query and return a list of matching devices use:

    usbquerydemo -e "<QUERY>"

replacing <QUERY> with the query string.

## Tree queries

Tree queries can match an entire hierarchy of USB devices. They are
specified with a YAML file, like this:

    query: usbid 1234:5667
    devices:
      leaf-1: usbid abcd:1200 && port 1
      leaf-2: usbid abcd:1201 && port 4
      child-hub:
        query: port 2
        devices:
          ...
    ---

The document is parsed as a device, which can either be a leaf device
(a string) or a hub device (a mapping specifying 'query' an 'devices').

Leaf devices are represented with a plain query string. Hub devices
include a query string and a child device mapping, which represents
the attached child devices the query is interested in. The list of
child devices is not exhaustive -- the hub can have other, unlisted
devices plugged in, but those will have no effect on query matching.

Tree queries essentially match each device according to its own query
and then ensuring the parent/child device links exist. In the example
above leaf-1 would be verified to have a parent with idProduct=1234
and idVendor=5567, etc. Similarily the root hub would check that all
three of its child devices exist and match their respective queries.

To test a tree query use:

    usbquery /path/to/query.yaml

Tree queries can match multiple device trees, and each of these will
be displayed as a separate hierarchy.
"""

from __future__ import annotations

import argparse
import io
from typing import Optional, cast

import strictyaml as sy
import usb.core

from tiab import usbquery
from tiab.utils import treeutils as tu


def main() -> None:
    parser = argparse.ArgumentParser(
        prog="usbquerydemo",
        description="test app for USB queries",
        epilog=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument(
        "file",
        nargs="?",
        type=argparse.FileType("r"),
        help="execute tree query from YAML description",
    )
    parser.add_argument(
        "-e",
        metavar="QUERY",
        nargs=1,
        type=str,
        help="execute query string from the command line",
    )

    args = parser.parse_args()
    all_devices = list(usb.core.find(find_all=True))

    file = cast(Optional[io.TextIOWrapper], args.file)
    if file:
        qtree_yaml = sy.load(file.read())
        qtree = usbquery.parse_tree(qtree_yaml)

        for dtree in usbquery.match_tree(qtree, all_devices):
            indent = 0

            for event in dtree.traverse():
                if event.type == tu.EventType.VISIT_PRE:
                    print("  " * indent, "*", repr(event.node.value))
                elif event.type == tu.EventType.LINK_PARENT:
                    indent -= 1
                elif event.type == tu.EventType.LINK_CHILD:
                    indent += 1

    query_strings = cast(Optional[list[str]], args.e)
    for query_str in query_strings or []:
        query = usbquery.parse(query_str)
        devices = list(usbquery.filter_match(query, all_devices))

        for device in devices:
            print(f"Device matched by {query_str!r}:")
            print(device)

        print("%d device(s) total" % len(devices))
