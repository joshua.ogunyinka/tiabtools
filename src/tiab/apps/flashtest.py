import argparse

import serial
from colorama import Fore, Style
from usb.core import find

CODETHINK_VENDOR_ID = 0x27BD
FTDI_VENDOR_ID = 0x0403
FTDI_PROD_ID = 0x6014

STM32_TIMEOUT = 3
STM32_BYTES_TO_READ = 10
STM32_PROMPT = "TIAB>"


def test_success(text: str) -> None:
    print(Fore.GREEN + "[OK]", Style.RESET_ALL + text)


def test_fail(text: str) -> None:
    print(Fore.RED + "[FAIL]", Style.RESET_ALL + text)


def find_unflashed_ft232h() -> None:
    not_flushed_ft232h = list(
        find(find_all=True, idVendor=FTDI_VENDOR_ID, idProduct=FTDI_PROD_ID),
    )
    if len(not_flushed_ft232h):
        print("Found some FT232H, maybe some of them have to be flashed:")
        for device in not_flushed_ft232h:
            print(f"{device.idVendor}:{device.idProduct}")
    else:
        print("There are no unflashed FT232H")


def test_ftdi() -> None:
    # for each Codethink FTDI device we store if we found it or not
    ct_ftdi_ids = {0x000A: False, 0x000B: False, 0x000C: False}
    flushed_ft232h = list(
        find(
            find_all=True,
            custom_match=lambda d: d.idVendor == CODETHINK_VENDOR_ID
            and d.idProduct in ct_ftdi_ids,
        ),
    )
    for device in flushed_ft232h:
        ct_ftdi_ids[device.idProduct] = True
    for k, v in ct_ftdi_ids.items():
        if not v:
            print(f"Codethink's FTDI with id={hex(k)} is missing")
        else:
            print(f"Codethink's FTDI with id={hex(k)} found")
    if False in ct_ftdi_ids.values():
        test_fail(
            "Some of the FTDI's are missing, trying to look for them somewhere else...",
        )
        find_unflashed_ft232h()
    else:
        test_success("All of the FTDI's were flashed correctly")


def test_stm32(stm_device) -> None:
    with serial.Serial(stm_device, timeout=STM32_TIMEOUT) as ser:
        # Read the prompt from the serial to be sure that our STM is flashed
        ser.write(b"\r\n")
        response = ser.read(STM32_BYTES_TO_READ).decode().strip()
        if response == STM32_PROMPT:
            test_success("STM32-C3 was flushed properly")
        else:
            test_fail(
                f"Something wrong with the STM: it returned '{response}' instead of 'TIAB>'",
            )


def run_ftdi_test() -> None:
    test_ftdi()


def run_stm32_test(devpath: str) -> None:
    try:
        test_stm32(devpath)
    except serial.SerialException as e:
        test_fail(f"STM Test Failed with error: {e}")


def main() -> None:
    parser = argparse.ArgumentParser(
        prog="flashtest",
        description="Test if TIAB hardware was flashed correctly",
    )
    parser.add_argument(
        "--stm",
        type=str,
        help="The STM32-C3 tty device",
        default="/dev/ttyACM0",
        metavar="<stm tty device>",
    )
    args = parser.parse_args()

    run_ftdi_test()
    run_stm32_test(args.stm)
