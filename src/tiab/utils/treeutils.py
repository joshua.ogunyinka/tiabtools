# Copyright (C) 2023 Codethink Ltd.

from __future__ import annotations

from dataclasses import dataclass
from enum import Enum, auto
from typing import TYPE_CHECKING, Generic, Literal, Protocol, TypeAlias, TypeVar, Union

if TYPE_CHECKING:
    from collections.abc import Iterator

# To avoid having a PLC0105 error from the linter, where all covariants are expected
# to be suffixed with _co, `Node` has been replaced with `Node_co`. However, the type
# has been aliased though.
Node_co = TypeVar("Node_co", covariant=True)
Node: TypeAlias = Node_co


class EventType(Enum):
    """Event types for tree traversals"""

    VISIT_IN = auto()
    VISIT_PRE = auto()
    VISIT_POST = auto()
    LINK_CHILD = auto()
    LINK_SIBLING = auto()
    LINK_PARENT = auto()


#: Event types used for `NodeEvent`s
NodeEventType = Literal[EventType.VISIT_IN, EventType.VISIT_PRE, EventType.VISIT_POST]

#: Event types used for `LinkEvent`s
LinkEventType = Literal[
    EventType.LINK_CHILD,
    EventType.LINK_SIBLING,
    EventType.LINK_PARENT,
]


@dataclass
class NodeEvent(Generic[Node_co]):
    """Emitted when visiting a tree node during a traversal."""

    #: Type of visit (in-order, pre-order, post-order)
    type: NodeEventType

    #: Node being visited
    node: Node_co


@dataclass
class LinkEvent(Generic[Node_co]):
    """Emitted when a traversal crosses a link/edge between two tree nodes."""

    #: Type of link being traversed
    type: LinkEventType

    #: Target node of the link, ie. the node about to be traversed
    node: Node_co

    #: Link origin node, ie. the node which we're moving away from
    prev: Node_co


#: Events emitted during a tree traversal
Event = Union[NodeEvent[Node_co], LinkEvent[Node_co]]


class Traversable(Protocol, Generic[Node_co]):
    """Protocol for traversable trees"""

    def traverse(self) -> Iterator[Event[Node_co]]:
        """
        Perform a depth-first tree traversal.

        Traversals are modeled as a sequence of events that are emitted at
        certain points during the traversal. Events can either visit nodes
        (`NodeEvent`s) or cross an edge between nodes (`LinkEvent`s), and
        these can be used to implement a variety of tree algorithms without
        knowing the specific details of the tree structure.

        Events are emitted strictly in the following order w.r.t. any tree
        node:

            earliest
            |
            | VISIT_PRE     node            \
            | LINK_CHILD    child, node     |  VISIT_IN node
            | LINK_PARENT   parent, node    |  validity range
            | VISIT_POST    node            /
            | LINK_SIBLING  sibling, node
            |
            V latest

        There are numerous additional rules not conveyed by this chart:

        - Each type of `VISIT` event is emitted exactly once for each node.

        - `VISIT_IN` can occur anywhere between `VISIT_PRE` and `VISIT_POST`
          and what it means depends on the implementation. It's permitted for
          it to immediately follow `VISIT_PRE` or precede `VISIT_POST`.

        - `LINK_CHILD child, node` implies that `child` is the first-visited
          child of `node` and any subsequent children will be reached through
          `LINK_SIBLING` events.

        - `LINK_SIBLING sibling, node` implies that `sibling` and `node`
          share a common parent and `sibling` is not the first-visited child.
          The ordering between siblings is left up to the implementation.

        - `LINK_PARENT parent, node` implies `node` is the last-visited
          child of `parent`, and implies that the entire subtree of `parent`
          has been visited.
        """

    def traverse_nodes(self, order: NodeEventType) -> Iterator[Node_co]:
        """
        Traverse tree nodes in some order.

        :param order: Used to select in-order, pre-order, or post-order
                      traversal.
        """

        def map_ev(ev: Event[Node_co]) -> Node_co:
            return ev.node

        def filter_ev(ev: Event[Node_co]) -> bool:
            return ev.type == order

        return map(map_ev, filter(filter_ev, self.traverse()))


class ParentTraversable(Protocol, Generic[Node_co]):
    """Protocol for trees that support arbitrary upward traversals"""

    def traverse_parents(self, *, include_self: bool = False) -> Iterator[Node_co]:
        """
        Iterate over the parents of this node, up to the root node.

        :param include_self: Whether `self` should be the first node
                             yielded by the iterator or skipped.
        """
