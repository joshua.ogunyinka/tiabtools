# Copyright (C) 2023 Codethink Ltd.

from __future__ import annotations

from typing import Optional, TypeVar, cast, get_args, get_origin

import strictyaml as sy

# TODO: This seems like an excessive amount of duplication.
# The obvious way of solving this is to split the scalar types
# out and then define YamlT in terms of it, but that doesn't
# work because Python doesn't allow unbound type variables as
# TypeVar bounds. In the meantime, this is ugly but it works.
YamlT = TypeVar(
    "YamlT",
    int,
    float,
    str,
    bool,
    list[int],
    list[float],
    list[str],
    list[bool],
)

SCALAR_VALIDATORS: dict[object, sy.Validator] = {
    int: sy.Int() | sy.HexInt(),
    float: sy.Float(),
    str: sy.Str(),
    bool: sy.Bool(),
}

# TODO: Extend this to mappings


def _get_validator(ty: object) -> sy.Validator:
    origin = cast(Optional[object], get_origin(ty))
    args = cast(tuple[object], get_args(ty))

    if origin is list and len(args) == 1:
        return sy.Seq(_get_validator(args[0]))

    if origin is None and ty in SCALAR_VALIDATORS:
        return SCALAR_VALIDATORS[ty]

    msg = f"Invalid yaml_cast type: {ty!r}"
    raise TypeError(msg)


def yaml_cast(ty: type[YamlT], yaml: sy.YAML) -> YamlT:
    """Validate YAML node and cast it to the specified data type."""

    # Create a copy of the YAML node because validation actually
    # mutates the input node. That would *normally* be harmless,
    # but this being library code it's best not to add surprises.
    new_yaml = sy.YAML(yaml)
    new_yaml.revalidate(_get_validator(ty))

    # Validation transforms the .data member into the desired type.
    return cast(YamlT, new_yaml.data)
