from PIL import Image, ImageDraw

ROWS_PER_PAGE = 8


def bits_to_byte(bit_arr: list[int]) -> int:
    res = 0
    for i in range(8):
        res |= bit_arr[i] << (8 - i - 1)
    return res


def encode_image(img) -> list[int]:
    w, h = img.size

    # convert image to byte array
    pxs = [1 if x > 0 else 0 for x in list(img.convert("1").getdata())]
    pixels = []

    # our small ssd1306 driver supports the vertical image encoding only. So we need to reformat the data
    # iterate over all pages
    for page in range(h // ROWS_PER_PAGE):
        # iterate over all image columns in a page
        for column in range(w):
            # iterate over ROWS_PER_PAGE pixels vertical block in the page
            for row_in_page in range(ROWS_PER_PAGE - 1, -1, -1):
                current_row = page * ROWS_PER_PAGE + row_in_page
                pixels.append(pxs[column + current_row * w])

    result = []

    # compress bits to bytes
    for i in range(len(pixels) // 8):
        num = bits_to_byte(pixels[i * 8 : (i + 1) * 8])
        result.append(num)

    result += [0] * (w * h - len(result))
    return result


def encode_text(text: str) -> list[int]:
    img = Image.new(mode="RGB", size=(128, 64))
    im = ImageDraw.Draw(img)
    im.text((10, 10), text, fill=(255, 255, 255))
    data = encode_image(img)
    img.close()
    return data
