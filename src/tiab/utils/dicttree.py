# Copyright (C) 2023 Codethink Ltd.

from __future__ import annotations

from collections.abc import Iterator, MutableMapping
from typing import Generic, TypeVar

import tiab.utils.treeutils as tu

K = TypeVar("K")
V = TypeVar("V")


class DictNode(
    Generic[K, V],
    MutableMapping[K, "DictNode[K, V]"],
    tu.Traversable["DictNode[K, V]"],
    tu.ParentTraversable["DictNode[K, V]"],
):
    """
    Dictionary-based tree node.

    A DictNode implements a tree by storing an arbitrary user value
    at each node and acting as a `MutableMapping` from user-defined
    keys to child nodes.

    Child nodes are added simply by assigning them to a key in the
    parent node:

        >>> parent = DictNode(1), child = DictNode(2)
        >>> parent['spam'] = child
        >>> child.parent is parent
        True
        >>> child.key
        'spam'

    You can remove a child node using `del`:

        >>> del parent['spam']
        >>> child.parent is None and child.key is None
        True

    Nodes are automatically removed from their existing parent when
    being assigned to a new parent.

    **Be warned that cycles are po NOT detected and will cause
    problems if you inadvertantly create one!!**
    """

    # TODO: DictNode improvements
    #
    # - Cycles should be detected and/or prevented
    #
    # - There should be a method to add a child if and only if it does
    #   not have a parent (no auto-reparenting)

    _parent: DictNode[K, V] | None
    _children: dict[K, DictNode[K, V]]
    _key: K | None
    value: V

    def __init__(self, value: V) -> None:
        self._parent = None
        self._children = {}
        self._key = None
        self.value = value

    def __getitem__(self, key: K) -> DictNode[K, V]:
        return self._children[key]

    def __setitem__(self, key: K, node: DictNode[K, V]) -> None:
        if node._parent and node._key:  # noqa: SLF001
            del node._parent[node._key]  # noqa: SLF001

        node._parent = self  # noqa: SLF001
        node._key = key  # noqa: SLF001
        self._children[key] = node

    def __delitem__(self, key: K) -> None:
        node = self._children[key]
        node._parent = None  # noqa: SLF001
        node._key = None  # noqa: SLF001

        del self._children[key]

    def __contains__(self, key: object) -> bool:
        return key in self._children

    def __iter__(self) -> Iterator[K]:
        return iter(self._children)

    def __len__(self) -> int:
        return len(self._children)

    def __repr__(self) -> str:
        if self._key:
            return f"<DictNode: {self._key!r} -> {self.value!r}>"

        return f"<DictNode: {self.value!r}>"

    @property
    def parent(self) -> DictNode[K, V] | None:
        return self._parent

    @property
    def key(self) -> K | None:
        return self._key

    def traverse(self) -> Iterator[tu.Event[DictNode[K, V]]]:
        yield tu.NodeEvent(tu.EventType.VISIT_PRE, self)
        yield tu.NodeEvent(tu.EventType.VISIT_IN, self)

        prevnode = self
        evtype: tu.LinkEventType = tu.EventType.LINK_CHILD

        for child in self._children.values():
            yield tu.LinkEvent(evtype, child, prevnode)

            yield from child.traverse()

            evtype = tu.EventType.LINK_SIBLING
            prevnode = child

        if prevnode is not self:
            yield tu.LinkEvent(tu.EventType.LINK_PARENT, self, prevnode)

        yield tu.NodeEvent(tu.EventType.VISIT_POST, self)

    def traverse_children(self) -> Iterator[DictNode[K, V]]:
        yield from self._children.values()

    def traverse_parents(
        self,
        *,
        include_self: bool = False,
    ) -> Iterator[DictNode[K, V]]:
        node = self if include_self else self._parent

        while node is not None:
            nextnode = node._parent  # noqa: SLF001
            yield node
            node = nextnode
