from pyftdi.i2c import I2cController, I2cPort

WIDTH = 128
HEIGHT = 64
BLOCK_SIZE = 16

SET_WIDTH_INTERVAL = 0x21
SET_PAGES_COUNT = 0x22
BLOCK_START = 0x40


class SSD1306Display:
    _controller: I2cController
    _dp: I2cPort

    def __init__(self, ftdi_url: str, i2c_addr: int) -> None:
        self._controller = I2cController()
        self._controller.configure(ftdi_url)
        self._dp = self._controller.get_port(i2c_addr)
        self._init_display()
        self.draw([0] * (WIDTH * HEIGHT))

    def _send_command(self, c: int) -> None:
        self._dp.write([0, c], start=True, relax=True)

    def draw(self, data: list[int]) -> None:
        data = data + [0] * (WIDTH * HEIGHT - len(data))

        self._send_command(SET_WIDTH_INTERVAL)
        self._send_command(0)
        self._send_command(WIDTH - 1)

        self._send_command(SET_PAGES_COUNT)
        self._send_command(0x00)
        self._send_command(HEIGHT // 8 - 1)

        i = 0
        while i < WIDTH * HEIGHT // 8:
            self._dp.write([BLOCK_START], start=True, relax=False)
            self._dp.write(data[i : i + BLOCK_SIZE], start=False, relax=True)
            i += BLOCK_SIZE

    def _init_display(self) -> None:
        init_seq = [
            0xAE,
            0xD5,
            0x80,
            0xA8,
            HEIGHT - 1,
            0xD3,
            0x00,
            0x40,
            0x8D,
            0x14,
            0x20,
            0x00,
            0xA1,
            0xC8,
            0xDA,
            0x12,
            0x81,
            0xCF,
            0xD9,
            0xF1,
            0xDB,
            0x40,
            0xA4,
            0xA6,
            0x2E,
            0xAF,
        ]

        for command in init_seq:
            self._send_command(command)

    def close(self) -> None:
        self._controller.close()
