# Copyright (C) 2023 Codethink Ltd.

from __future__ import annotations

from dataclasses import dataclass, field

from tiab.usbquery import Query, QueryNode


@dataclass
class UsbSerialDesc:
    """Describes a serial port attached via a USB device."""

    #: Name of the USB device (in `HardwareDesc`) providing the port.
    device: str

    # TODO: add interface number for handling devices with multiple ports
    # TODO: add a flag for ports that can come and go at runtime


#: Union of all serial port description types.
SerialDesc = UsbSerialDesc


@dataclass
class HardwareDesc:
    """
    Describes how to identify a collection of related hardware resources
    like USB devices and serial ports.
    """

    #: Mapping of USB device name -> USB query
    usb_queries: dict[str, Query] = field(default_factory=dict)

    #: Mapping of child USB device name -> parent device name
    usb_parents: dict[str, str] = field(default_factory=dict)

    #: Maps serial device IDs -> descriptions
    serial_descs: dict[str, SerialDesc] = field(default_factory=dict)

    def get_usb_tree_queries(self) -> dict[str, QueryNode]:
        """Return USB query trees for identifying all devices"""

        nodes: dict[str, QueryNode] = {}

        # Build node list
        for name, query in self.usb_queries.items():
            nodes[name] = QueryNode(query)

        # Assign parent of each node
        # TODO: This should do cycle detection
        for child, parent in self.usb_parents.items():
            child_node = nodes[child]
            parent_node = nodes[parent]

            parent_node[child] = child_node

        # Return any remaining root nodes
        return {name: node for name, node in nodes.items() if node.parent is None}
