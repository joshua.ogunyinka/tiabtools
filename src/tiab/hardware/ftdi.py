# Copyright (C) 2023 Codethink Ltd.

from __future__ import annotations

from dataclasses import dataclass, field
from typing import TYPE_CHECKING, TypedDict, Union, cast

from pyftdi.eeprom import FtdiEeprom

if TYPE_CHECKING:
    from pathlib import Path

    from usb.core import Device as UsbDevice

EEPROM_INI_RAW: str = "raw"
EEPROM_INI_VALUES: str = "values"
EEPROM_INI_ALL: str = "all"


class FtdiFirmwareProps(TypedDict, total=False):
    """Partial list of configurable EEPROM settings."""

    vendor_id: int
    product_id: int
    manufacturer: str
    product: str
    serial: str
    cbus_func_0: str
    cbus_func_1: str
    cbus_func_2: str
    cbus_func_3: str
    cbus_func_4: str
    cbus_func_5: str
    cbus_func_6: str
    cbus_func_7: str
    cbus_func_8: str
    cbus_func_9: str
    self_powered: bool
    power_max: int


@dataclass
class FtdiFirmwareConfig:
    """Firmware configuration data for FTDI EEPROMs."""

    #: List of properties to set, overrides any values set in the .ini file.
    properties: FtdiFirmwareProps = field(default_factory=lambda: FtdiFirmwareProps())

    #: Path to an PyFTDI EEPROM .ini file.
    ini_path: Path | None = None

    #: Which .ini file sections to use: 'raw', 'values', or 'all'.
    ini_section: str | None = None

    #: Specify a custom EEPROM size, in bytes.
    eeprom_size: int | None = None

    #: If True, existing EEPROM data is ignored and new data is
    #: defined solely by this configuration. Normally, you want
    #: to leave this as False and only update fields you need to
    #: change, so other fields will not be modified.
    ignore_existing_data: bool = False


class FtdiFirmwareIO:
    """Firmware flasher & dumper for FTDI EEPROMs"""

    _config: FtdiFirmwareConfig
    _eeprom: FtdiEeprom

    def __init__(self, config: FtdiFirmwareConfig) -> None:
        self._config = config
        self._eeprom = FtdiEeprom()

    def open(self, device: UsbDevice) -> None:
        self._eeprom.open(
            device,
            ignore=self._config.ignore_existing_data,
            size=self._config.eeprom_size,
        )

    def close(self) -> None:
        self._eeprom.close()

    def update_firmware(self, *, dry_run: bool = True) -> None:
        """Update EEPROM using the provided config"""

        if self._config.ini_path:
            with self._config.ini_path.open("r") as f:
                self._eeprom.load_config(f, self._config.ini_section)

        for prop, value in self._config.properties.items():
            # XXX: It would be nicer if PyFTDI handled these in .set_property()
            if prop == "manufacturer":
                self._eeprom.set_manufacturer_name(cast(str, value))
            elif prop == "product":
                self._eeprom.set_product_name(cast(str, value))
            elif prop == "serial":
                self._eeprom.set_serial_number(cast(str, value))
            else:
                # Unfortunately, mypy cannot deduce the type of "value" at all
                # so we need to cast it.
                self._eeprom.set_property(prop, cast(Union[str, int, bool], value))

        self._eeprom.commit(dry_run=dry_run)

    def dump_firmware(self) -> None:
        """
        Read EEPROM contents and populate the config with current data.

        If `config.ini_path` is defined, an .ini file will be written
        reflecting the current EEPROM contents. All other config fields
        will be updated to match EEPROM contents.
        """

        # TODO: Read properties from EEPROM
        self._config.properties = FtdiFirmwareProps()
        self._config.eeprom_size = self._eeprom.storage_size
        self._config.ignore_existing_data = False

        if self._config.ini_path:
            with self._config.ini_path.open("w") as f:
                self._eeprom.save_config(f)
            self._config.ini_section = EEPROM_INI_ALL
        else:
            self._config.ini_section = None

    @property
    def firmware_config(self) -> FtdiFirmwareConfig:
        return self._config
