from time import sleep

from pyftdi.spi import SpiController, SpiPort


class BMP280:
    _spi: SpiController
    _slave: SpiPort
    _T1: float
    _T2: float
    _T3: float

    def _write_b(self, register: int, value: int) -> None:
        register &= 0x7F
        self._slave.write(bytes([register, value & 0xFF]), start=True, stop=True)

    def _read8(self, register: int) -> int:
        return self._slave.exchange([register | 0x80], 1)[0]

    def _read16(self, register: int) -> int:
        result = self._slave.exchange([register | 0x80], 2)
        return result[0] << 8 | result[1]

    def _read16s(self, register: int) -> int:
        result = self._read16(register)
        if result >= 2**16:
            result -= 2**16
        return result

    def _read16_le(self, register: int) -> int:
        result = self._slave.exchange([register | 0x80], 2)
        return result[1] << 8 | result[0]

    def _read16s_le(self, register: int) -> int:
        result = self._read16_le(register)
        if result >= 2**16:
            result -= 2**16
        return result

    def _read24(self, register: int) -> int:
        result = self._slave.exchange([register | 0x80], 3)
        return result[0] << 16 | result[1] << 8 | result[2]

    def _read_coef(self) -> None:
        self._T1 = self._read16_le(0x88)
        self._T2 = self._read16s_le(0x8A)
        self._T3 = self._read16s_le(0x8C)

    def __init__(self, ftdi_addr: str, cs: int) -> None:
        self._spi = SpiController()
        self._spi.configure(ftdi_addr)
        self._slave = self._spi.get_port(cs)
        self._slave.set_frequency(10**6)
        self._read_coef()

    def _force_read(self) -> None:
        self._slave.exchange([0xF4 & ~0x80, 0xFE])

    def read_temp(self) -> float:
        self._force_read()

        while self._read8(0xF3) & 0x08:
            sleep(0.01)

        adc_t = float(self._read24(0xFA) >> 4)

        v1 = ((adc_t / 16384) - (self._T1 / 1024)) * self._T2
        v2 = (
            (adc_t / 131072.0 - self._T1 / 8192) * (adc_t / 131072.0 - self._T1 / 8192)
        ) * self._T3

        t_fine = v1 + v2

        return t_fine / 5120.0

    def close(self) -> None:
        self._spi.close()
