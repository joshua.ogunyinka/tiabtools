# Copyright (C) 2023 Codethink Ltd.

from __future__ import annotations

from typing import TYPE_CHECKING, cast

from serial import Serial
from serial.tools.list_ports import comports
from usb.core import Device
from usb.core import find as find_usb

from tiab.usbquery import DeviceNode, QueryNode, match_tree
from tiab.utils import treeutils

from .desc import HardwareDesc, SerialDesc, UsbSerialDesc

if TYPE_CHECKING:
    from collections.abc import Iterable

    from serial.tools.list_ports_common import ListPortInfo

# TODO: It would be really useful if HardwareUnit was a bit more encapsulated
# and able to eg. re-scan attached USB devices to recover from a disconnect.
# The way things are now, it's hard to do this without external machinery; you
# need to look at serial numbers or port numbers or something. In theory
# we could augment the HardwareDesc queries with that info automatically.


class HardwareError(Exception):
    """Error when a hardware unit goes into an unexpected state"""


class HardwareUnit:
    """Container for accessing a collection of related hardware resources"""

    _desc: HardwareDesc
    _usb_devices: dict[str, Device]
    _serial_ports: dict[str, Serial]

    def __init__(self, desc: HardwareDesc, usbs: dict[str, Device]) -> None:
        self._desc = desc
        self._usb_devices = usbs
        self._serial_ports = {}

        self._scan_serial_ports()

    def _scan_serial_ports(self) -> None:
        # This scans the system for serial ports and creates a pyserial object
        # for each of them. TODO: this is a bit inflexible if serial ports can
        # appear and disappear at runtime. Probably some ports need to be made
        # optional so them not existing is not an error -- and when rescanning
        # due to a config change, we want to make sure the expected ports are
        # appearing/disappearing.

        def _usb_provides_port(dev: Device, port: ListPortInfo) -> bool:
            # Workaround for dumb type stubs
            vid = cast(object, port.vid)
            pid = cast(object, port.pid)

            if not isinstance(vid, int) or not isinstance(pid, int):
                return False

            # TODO: This has to be *much* more specific, likely parsing Linux sysfs.
            return vid == dev.idVendor and pid == dev.idProduct

        def _match_port(
            unit: HardwareUnit,
            sdesc: SerialDesc,
            port: ListPortInfo,
        ) -> bool:
            if isinstance(sdesc, UsbSerialDesc):
                dev = unit.get_usb_device(sdesc.device)

                # TODO: this should check interface number, etc.
                return _usb_provides_port(dev, port)

        self._serial_ports.clear()

        ports: list[ListPortInfo] = list(comports())

        for name, sdesc in self._desc.serial_descs.items():
            matched_ports = [port for port in ports if _match_port(self, sdesc, port)]

            if len(matched_ports) == 0:
                msg = f"Serial port {name!r} not found"
                raise HardwareError(msg)

            if len(matched_ports) > 1:
                msg = f"Serial port {name!r} matched multiple ports:"
                raise HardwareError(msg)

            serial = Serial()
            serial.port = matched_ports[0].device

            self._serial_ports[name] = serial

    @property
    def desc(self) -> HardwareDesc:
        return self._desc

    def get_usb_device(self, name: str) -> Device:
        return self._usb_devices[name]

    def get_serial_port(self, name: str) -> Serial:
        return self._serial_ports[name]


def _flatten_usb_device_tree(trees: dict[str, DeviceNode]) -> dict[str, Device]:
    """
    Flattens a forest of USB device trees into a dict. This assumes
    that all devices have unique keys across all trees.
    """

    devices: dict[str, Device] = {}

    for root_name, root_node in trees.items():
        devices[root_name] = root_node.value

        for node in root_node.traverse_nodes(treeutils.EventType.VISIT_PRE):
            if node.key:
                devices[node.key] = node.value

    return devices


def _get_usb_devices(qtrees: dict[str, QueryNode]) -> Iterable[dict[str, Device]]:
    """
    Query attached USB devices and return a device map for each hardware unit.
    """

    device_list = list(find_usb(find_all=True))
    dtree_maps: list[dict[str, DeviceNode]] = []

    for name, qtree in qtrees.items():
        dtrees = match_tree(qtree, device_list)

        # It's not possible to disambiguate multiple matches when there are
        # multiple root devices in the hardware description. This is mostly
        # a USB 3.0 problem, where the same hub shows up as two root devices,
        # one on the 3.0 bus and one on the 2.0 bus, so we can avoid dealing
        # with it seriously right now.
        if len(qtrees) > 1 and len(dtrees) > 1:
            return []

        # Create a root mapping for each matched tree
        while len(dtree_maps) < len(dtrees):
            dtree_maps.append({})

        # Add entry for this tree to each root mapping
        for dtree in dtrees:
            for dtree_map in dtree_maps:
                dtree_map[name] = dtree

    return map(_flatten_usb_device_tree, dtree_maps)


def open_hw(desc: HardwareDesc) -> list[HardwareUnit]:
    """
    Open all hardware units matching the given description.
    """

    units: list[HardwareUnit] = []

    # TODO: Catch errors when necessary serial ports aren't found, etc.
    for devmap in _get_usb_devices(desc.get_usb_tree_queries()):
        unit = HardwareUnit(desc, devmap)
        units.append(unit)

    return units
