# Copyright (C) 2023 Codethink Ltd.

from __future__ import annotations

from typing import cast

import strictyaml as sy

from tiab import usbquery
from tiab.utils.yaml import yaml_cast

from .desc import HardwareDesc, SerialDesc, UsbSerialDesc

# Schema for UsbSerialDesc
USB_SERIAL_DESC_SCHEMA = sy.Map(
    {
        "type": sy.Enum({"usb"}),
        "device": sy.Str(),
    },
)

# Schema for any SerialDesc
SERIAL_DESC_SCHEMA = sy.MapCombined(
    {
        "type": sy.Enum({"usb"}),
    },
    sy.Str(),
    sy.Any(),
)

# Schema for HardwareDesc
HARDWARE_DESC_SCHEMA = sy.Map(
    {
        sy.Optional("usb-queries"): sy.MapPattern(sy.Str(), sy.Str()),
        sy.Optional("usb-parents"): sy.MapPattern(sy.Str(), sy.Str()),
        sy.Optional("serial-descs"): sy.MapPattern(sy.Str(), SERIAL_DESC_SCHEMA),
    },
)


def parse_serial_desc(yaml: sy.YAML) -> SerialDesc:
    yaml.revalidate(SERIAL_DESC_SCHEMA)

    desc_type = yaml_cast(str, yaml["type"])

    if desc_type == "usb":
        device_name = yaml_cast(str, yaml["device"])

        return UsbSerialDesc(device=device_name)

    msg = "YAML schema error"
    raise AssertionError(msg)


def parse_hardware_desc(yaml: sy.YAML) -> HardwareDesc:
    hwdesc = HardwareDesc()
    yaml.revalidate(HARDWARE_DESC_SCHEMA)

    if "usb-queries" in yaml:
        query_map = cast(dict[str, str], yaml["usb-queries"].data)
        for name, query_str in query_map.items():
            hwdesc.usb_queries[name] = usbquery.parse(query_str)

    if "usb-parents" in yaml:
        parent_map = cast(dict[str, str], yaml["usb-parents"].data)
        for child_name, parent_name in parent_map.items():
            hwdesc.usb_parents[child_name] = parent_name

    if "serial-descs" in yaml:
        serial_descs = yaml["serial-descs"]
        for sdesc_name_yaml in serial_descs:
            sdesc_name = yaml_cast(str, sdesc_name_yaml)
            sdesc_yaml = serial_descs[sdesc_name_yaml]

            hwdesc.serial_descs[sdesc_name] = parse_serial_desc(sdesc_yaml)

    return hwdesc
