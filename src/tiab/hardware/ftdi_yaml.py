# Copyright (C) 2023 Codethink Ltd.

from __future__ import annotations

from pathlib import Path

import strictyaml as sy

from tiab.utils.yaml import yaml_cast

from .ftdi import (
    EEPROM_INI_ALL,
    EEPROM_INI_RAW,
    EEPROM_INI_VALUES,
    FtdiFirmwareConfig,
    FtdiFirmwareProps,
)

PROPS_SCHEMA_DICT = {
    "vendor-id": sy.HexInt(),
    "product-id": sy.HexInt(),
    "manufacturer": sy.Str(),
    "product": sy.Str(),
    "serial": sy.Str(),
    "cbus-func-0": sy.Str(),
    "cbus-func-1": sy.Str(),
    "cbus-func-2": sy.Str(),
    "cbus-func-3": sy.Str(),
    "cbus-func-4": sy.Str(),
    "cbus-func-5": sy.Str(),
    "cbus-func-6": sy.Str(),
    "cbus-func-7": sy.Str(),
    "cbus-func-8": sy.Str(),
    "cbus-func-9": sy.Str(),
    "self-powered": sy.Bool(),
    "power-max": sy.Int(),
}

# Schema for FtdiFirmwareProps
FTDI_PROPS_SCHEMA = sy.Map({sy.Optional(k): v for k, v in PROPS_SCHEMA_DICT.items()})

# Schema for FtdiFirmareConfig
FTDI_FIRMWARE_CONFIG_SCHEMA = sy.Map(
    {
        sy.Optional("properties"): FTDI_PROPS_SCHEMA,
        sy.Optional("ini-path"): sy.Str(),
        sy.Optional("ini-section"): sy.Enum(
            {EEPROM_INI_ALL, EEPROM_INI_RAW, EEPROM_INI_VALUES},
        ),
        sy.Optional("eeprom-size"): sy.Int(),
        sy.Optional("ignore-existing-data"): sy.Bool(),
    },
)


def parse_ftdi_props(yaml: sy.YAML) -> FtdiFirmwareProps:
    yaml.revalidate(FTDI_PROPS_SCHEMA)
    props = FtdiFirmwareProps()

    for schema_key in PROPS_SCHEMA_DICT:
        if schema_key in yaml:
            props_key = schema_key.replace("-", "_")
            props[props_key] = yaml[schema_key].data  # type: ignore [literal-required, misc]

    return props


def parse_ftdi_firmware_config(yaml: sy.YAML) -> FtdiFirmwareConfig:
    yaml.revalidate(FTDI_FIRMWARE_CONFIG_SCHEMA)
    config = FtdiFirmwareConfig()

    if "properties" in yaml:
        config.properties = parse_ftdi_props(yaml["properties"])

    if "ini-path" in yaml:
        config.ini_path = Path(yaml_cast(str, yaml["ini-path"]))

    if "ini-section" in yaml:
        config.ini_section = yaml_cast(str, yaml["ini-section"])

    if "eeprom-size" in yaml:
        config.eeprom_size = yaml_cast(int, yaml["eeprom-size"])

    if "ignore-existing-data" in yaml:
        config.ignore_existing_data = yaml_cast(bool, yaml["ignore-existing-data"])

    return config
