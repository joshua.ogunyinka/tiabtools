# Development

Create a Python virtual environment:

```
python3 -m venv .venv
source .venv/bin/activate
pip install -U pip
pip install -e .[dev,types,tests]
```

Setup pre-commit, but note this only checks basics like trailing whitespace:

```
pre-commit install
```

You should at minimum run `tox r -e lint` and `tox r -e type` to ensure your
changes pass linters and typechecking. TODO: write a git hook that does this
automatically.

## Type checking

Preferably, all code should have type annotations and typecheck successfully,
but a lot of dependencies don't have type annotations. At this time, stubs are
being maintained locally under `stubs/` - if a 3rd-party library causes problems
in typechecking, it's a sign the stubs need to be improved.

## Linting

Currently `ruff` is used for linting since it's fast and implements a large
number of checks. For now all checks are enabled and less useful ones are
disabled on a case by case basis.

## Testing

This is done through `tox` to ensure the package is installable and works on all
supported Python versions. To run tests:

- `tox r` runs tests under all environments serially
- `tox p` tests all environments in parallel
- `tox r -e ENV` to run a specific environment (eg. `py311`, `py39`)

See `tox.ini` for configuration.

### Code coverage

This has to be done manually by running pytest against a specific Python
version. TODO: have tox do this automatically and gather coverage across all
python versions to catch any differences.

- `pytest --cov=tiab tests` to generate coverage data
- `coverage html` to generate an HTML report

# Flashing instructions

Use `tiabflash` for flashing devices:

```
tiabflash --config <device-config.yaml>
```

The flash procedure is described in a YAML config file. An example for
testing-in-a-box rev A boards can be found in examples/tiab-rev-a.yaml.

On Linux you will need sufficient privileges to access the USB devices. Running
`tiabflash` as root is not recommended, instead you should grant access to an
unprivileged user with udev rules. Sample udev rules are provided in
`etc/11-tiab-ftdi.rules`. Copy them to `/etc/udev/rules.d` and make sure your
user is a member of the `plugdev` group, or adapt them as needed.
