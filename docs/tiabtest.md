## TIAB flashing testing tool

We assume that the flashing process was successful if:

1. The vendor and product ids of the FTDI devices are correct
1. SMT32 replies to an empty command with a correct prompt

The TIAB testing tool performs these checks automatically in the following
order:

1. Firstly it iterates over all Codethink USB devices and checks all of FTDIs
    for presence.
1. If it is not able to find the one of the FTDIs, it looks for unflashed FTDIs
    in the system and prints them out.
1. After testing the FTDI devices, it checks if the STM replies to an empty
    command with a correct prompt.

To run the TIAB testing tool, run `tiabtest` in the project directory.
