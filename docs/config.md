# Configuration Files for Tiab Tools

To configure Testing in a Box I/O board, you will need to create a configuration
file for it. There are examples in `examples/` folder in the Tiab Tools repo.

Here is an example of a configuration file from `examples/ftdi-flash.yml`:

```yaml
hardware:
  usb-queries:
    ftdi: usbid 0403:6014

flash-steps:
  - action: flash-ftdi-eeprom
    device: ftdi
    config:
      properties:
        manufacturer: Codethink
        product: FTDI serial adapter
        serial: EXAMPLE
  - action: usb-reset
    devices:
      - ftdi
```

## `hardware`

This is where all the hardware is defined.

### `usbqueries`

This is where you will define a list of hardware to use with its product ID and
vendor ID. Using `name-of-chip: {{{ vendor_id }}:{{ product_id }}`

## `flash-steps`

This is where the list of tasks to do to flash board. Currently these are the
actions available to use:

### `flash-steps.action`

These are the following actions currently supported:

| Action name       | Description                                                                           |
| ----------------- | ------------------------------------------------------------------------------------- |
| flash-ftdi-eeprom | Flash the an FTDI chips EEPROMs to a specified product ID, vendor ID and manufacturer |
| usb-reset         | Reset specified USB device                                                            |

### `flash-steps.action.device`

List of devices to execute the action to.

### Action parameters

Some actions require specific parameters to execute.

#### `flash-ftdi-eeprom`

##### `flash-steps.action.config`

Configuration for `flash-ftdi-eeprom` action.

#### `flash-steps.action.config.properties`

The properties for the FTDI chips configuration

#### `flash-steps.action.config.properties.manufacturer`

Specify the manufacture of the chip to change it.

#### `flash-steps.action.config.properties.product`

Specify the product ID to change to.

#### `flash-steps.action.config.properties.serial`

Specify the serial ID to change to.
