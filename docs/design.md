# Flashing overview

1. Flash all 3 FTDI EEPROMs
    - FTDI1: set CBUS8/9 to bitbang mode
    - FTDI2/3: no special options afaict
1. Enter UART mode on FTDI1
1. Select RP2040 and flash it
1. Select ESP32 and flash it
1. This may vary for rev A and rev B depending on how the USB hubs behave:
    - If EEPROM flashing causes hubs to flake out:
        - Power up ESP32
        - Flash hub2 EEPROM from ESP32
        - Flash hub1 EEPROM from ESP32
        - System reset (ESP32 triggered?)
    - Otherwise for rev B, we can tie EEPROMs to FTDI1's I2C bus and flash both
        EEPROMs directly from the host, but we'd still need a jumper to isolate
        them when FTDI1's I2C bus is being used for another purpose.

### FTDI EEPROMs

EEPROM flashing is now implemented in tiabflash, see examples/ftdi-flash.yaml.

The only hitch is that reprogramming the USB VID/PID breaks old device handles
so the `HardwareUnit` will need to re-acquire those. That isn't implemented yet.

### Serial access on FTDI1

We need serial access on FTDI1 to flash the ESP32 and RP2040. This can be done
through PyFTDI (which will be janky) or through a `/dev/ttyX` device, which is a
lot more portable.

The problem is we need to control CBUS8/9 to switch the serial endpoint between
ESP32/RP2040. Doing that from PyFTDI will detach the `ftdi_sio` kernel drive and
thus bring down the tty device. `ftdi_sio` does provide control over the CBUS
pins through a GPIO chardev (`/dev/gpiochipX`) and that looks like the best
option because it won't affect the tty device, the only downside is it means
more code and additional dependencies.

### RP2040

From a very cursory look it seems we could start OpenOCD pointing at the serial
port and go from there.

### ESP32

This should be easy with https://github.com/espressif/esptool which has a
reasonable looking API. It seems we just need to pass the serial port and things
should work automatically.

### USB hub EEPROMs

This is where flashing gets tricky because the only access we have to the hub
EEPROMs is through the ESP32's I2C bus.

The firmware will need to provide an interface on one of its serial ports for
reading, erasing, and writing hub EEPROMs. How this looks will depend on what
happens to the USB hub when its EEPROM is rewritten.

# Normal operation

1. Switch FTDI1 to MPSSE mode
    - I2C breakout
    - 1 GPIO output to ESP32 (needed for UART boot but useless in MPSSE)
    - 8x LED outputs?
1. FTDI2/3 are fully broken out but will operate only in MPSSE mode initially
1. RP2040 only a USB 12Mbps device, accessible _only_ through the ESP32
1. ESP32 only controls an LED?
