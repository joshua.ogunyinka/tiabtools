## TIAB Temperature-to-Display Tool

tiab-temp2display is a sample tool which can be used for reading the temperature
from a BMP280 SPI sensor and displaying it on a SSD1306 I2C oled display.

### Example

Run the following command to read the temperature from the sensor (0x000c FTDI
device) and display it on the screen (0x000a FTDI device) with 2 seconds
interval between temperature updates:

`tiab-temp2display --sensor ftdi://0x27bd:0x000c/1 --display ftdi://0x27bd:0x000a/1 --interval 2`

### Command-line Arguments

- `--sensor` - the BMP280 FTDI url
- `--display` - the SSD1306 FTDI url
- `--interval` - the interval between the temperature updates
